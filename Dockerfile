FROM nginx:latest
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY dist/angular-ci/ .
COPY nginx.conf /etc/nginx/nginx.conf
